/** Allows for the usage of the humanGeography class through the command line.
 * 
 * @author Milo Gilad
 * @version 5/12/17
 */
import java.util.Scanner;
public class humanGeoInterface {
    static void calculator() {
        for (int i = 0; i<10; i++) {
            System.out.println();
        }
        System.out.println("Type in one of the follwing: ");
        System.out.println("CBR");
        System.out.println("CDR");
        System.out.println("RNI");
        System.out.println("DoublingTime");
        System.out.println("NMR");
        System.out.println("PopGrowth");
        System.out.print("or TFR (Control+C to exit): ");
        Scanner scan = new Scanner(System.in);
        String response = scan.next();
        try {
            Thread.sleep(500);
        } catch (InterruptedException ie) {
            System.out.println(ie);
        }
        try {
            switch(response) {
                case "CBR" :
                    System.out.print("Enter in the live births in the population: ");
                    long liveBirths = scan.nextLong();
                    System.out.print("Enter in the total population: ");
                    long totalPop = scan.nextLong();
                    System.out.println(humanGeography.cbr(liveBirths, totalPop));
                    break;
                case "CDR" :
                    System.out.print("Enter in the deaths in the population: ");
                    long deaths = scan.nextLong();
                    System.out.print("Enter in the total population: ");
                    totalPop = scan.nextLong();
                    System.out.println(humanGeography.cbr(deaths, totalPop));
                    break;
                case "RNI" :
                    System.out.print("Enter in the CBR of the population: ");
                    double cbr = scan.nextDouble();
                    System.out.print("Enter in the CDR of the population: ");
                    double cdr = scan.nextDouble();
                    System.out.println(humanGeography.rni(cbr, cdr));
                    break;
                case "DoublingTime" :
                    System.out.print("Enter in the RNI of the population: ");
                    double rni = scan.nextDouble();
                    System.out.println(humanGeography.doubleTime(rni));
                    break;
                case "NMR" :
                    System.out.print("Enter in the number of immigrants to the population: ");
                    long immigrants = scan.nextLong();
                    System.out.print("Enter in the number of emigrants from the population: ");
                    long emigrants = scan.nextLong();
                    System.out.print("Enter in the population: ");
                    long population = scan.nextLong();
                    System.out.println(humanGeography.nmr(immigrants, emigrants, population));
                    break;
                case "PopGrowth" :
                    System.out.print("Would you like the growth calculated in percent (p) or decimal (d) form? ");
                    String pOrD = scan.next();
                    switch (pOrD) {
                        case "p" :
                            System.out.print("Enter the CBR: ");
                            cbr = scan.nextDouble();
                            System.out.print("Enter the CDR: ");
                            cdr = scan.nextDouble();
                            System.out.print("Enter the NMR: ");
                            double nmr = scan.nextDouble();
                            System.out.println(humanGeography.popGrowthPercent(cbr, cdr, nmr));
                            break;
                        case "d" : 
                            System.out.print("Enter the CBR: ");
                            cbr = scan.nextDouble();
                            System.out.print("Enter the CDR: ");
                            cdr = scan.nextDouble();
                            System.out.print("Enter the NMR: ");
                            nmr = scan.nextDouble();
                            System.out.println(humanGeography.popGrowthDouble(cbr, cdr, nmr));
                            break;
                    }
                    break;
                case "TFR" :
                    System.out.print("Enter the CBR: ");
                    cbr = scan.nextDouble();
                    System.out.print("Enter the amount of women aged 15 through 45 in the population: ");
                    long women15_45 = scan.nextLong();
                    System.out.println(humanGeography.tfr(cbr, women15_45));
                    break;
                default: 
                    break;
            }
        } catch (Throwable t) {
            System.out.println("\n" + "There was an issue with the inputed data. Maybe you typed a letter by accident?");
        }  
    }
    public static void main(String args[]) {
        while(true){
            calculator();
            try {
                Thread.sleep(2000);
                System.out.println();
            } catch (InterruptedException ie) {
                System.out.println(ie);
        }
        }
    }
}