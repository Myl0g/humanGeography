
/**
 * Contains many tools for calculating various statistics used by Human Geographers.
 * 
 * @author Milo Gilad
 * @version 5/12/17
 */
public class humanGeography
{
    /** Calculates the Crude Birth Rate (CBR) from provided data. If the returned value is 0.0, then the number is too small to display.
     * Formula: (number of live births divided by total population) times 1000 
     * @param liveBirths Number of live births in a population.
     * @param totalPop The total population.
     */
    final public static double cbr(long liveBirths, long totalPop) {return (liveBirths / totalPop) * 1000;}
    /** Calculates the Crude Death Rate (CDR) from provided data. If the returned value is 0.0, then the number is too small to display.
     * Formula: (number of deaths divided by total population) times 1000
     * @param deaths Number of deaths in a population.
     * @param totalPop The total population.
     */
    final public static double cdr(long deaths, long totalPop) {return (deaths / totalPop) * 1000;}
    /** Calculates the Rate of Natural Increase (RNI) from provided data.
     * Formula: (birth rate minus death rate) divided by 10
     * @param cbr Crude Birth Rate (calculate using cbr method)
     * @param cdr Crude Death Rate (calculate using cdr method)
     */
    final public static double rni(double cbr, double cdr) {return (cbr - cdr) / 10;}
    /** Calculates the doubling time of a population (that is, how long it takes for the population to double).
     * Formula: 70 divided by RNI
     * @param rni Rate of Natural Increase (RNI)
     */
    final public static double doubleTime(double rni) {return 70/rni;}
    /** Calculates Net Migration Rate from provided data.
     * Formula: (number of immigrants minus number of emigrants) divided by (population divided by 1000)
     * @param immigrants Number of immigrants to a population.
     * @param emigrants Number of emigrants to a population.
     * @param population Population.
     */
    final public static double nmr(long immigrants, long emigrants, long population) {return (immigrants-emigrants)/(population/1000);}
    /** Calculates the population's growth as a decimal.
     * Formula: ([birth rate - death rate] + NMR) divided by 10
     * @param cbr Crude Birth Rate (calculate using cbr method)
     * @param cdr Crude Death Rate (calculate using cdr method)
     * @param nmr Net Migration Rate
     */
    final public static double popGrowthDouble(double cbr, double cdr, double nmr) {return ((cbr - cdr) + nmr) / 10;}

    /** Calculates the population's growth as a percent.
     * Formula: ([birth rate - death rate] + NMR) divided by 10
     * @param cbr Crude Birth Rate (calculate using cbr method)
     * @param cdr Crude Death Rate (calculate using cdr method)
     * @param nmr Net Migration Rate
     */
    final public static String popGrowthPercent(double cbr, double cdr, double nmr) {return (popGrowthDouble(cbr, cdr, nmr) * 100) + "%";}
    /** Calculates the Total Fertility Rate (TFR) from provided data.
     * Formula: CBR divided by women age 15-45
     * @param cbr Crude Birth Rate (calculate using cbr method)
     * @param women15_45 All women aged 15 through 45 in a population.
     */
    final public static double tfr(double cbr, long women15_45) {return cbr/women15_45;}
}
